package t.core.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import t.core.entity.UserEntity;
import t.core.exception.ServiceException;
import t.core.service.UserService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/admin")
public class AdminController {

    private final UserService userService;

    @GetMapping("/all")
    public ResponseEntity<List<UserEntity>> getAllUsers(@RequestParam Boolean disable){
        return ResponseEntity.ok(userService.getAllUserWithStatus(disable));
    }

    //todo добавть метод для получения всех изображений пользователя


    @PatchMapping("/user")
    public ResponseEntity<String> disableUser(@RequestParam String userEmail) throws ServiceException {
        userService.disableUser(userEmail);
        return ResponseEntity.ok("User was disable");
    }
}
