package t.core.controller;


import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import t.core.dto.FileInfoDto;
import t.core.dto.UserDto;
import t.core.entity.FileInfo;
import t.core.entity.UserEntity;
import t.core.exception.ServiceException;
import t.core.security.UserPrincipal;
import t.core.service.UploadService;
import t.core.service.UserService;

import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class UserController {

    private final UserService userService;
    private final UploadService uploadService;

    @PostMapping("/register")
    public ResponseEntity<String> registerUser(@Validated @RequestBody UserDto userDto) throws ServiceException {
        return ResponseEntity.ok("Registration successful: " + userService.addUserEntity(userDto));
    }

    @DeleteMapping(path = "/user", produces = {"application/json"})
    public void deleteUser(@AuthenticationPrincipal UserPrincipal userPrincipal) throws ServiceException {
        userService.removeUserEntity(userPrincipal.getUserId());
    }

    @GetMapping(path = "/user", produces = {"application/json"})
    public ResponseEntity<UserEntity> getUserEntity(@AuthenticationPrincipal UserPrincipal userPrincipal) throws ServiceException {
        return ResponseEntity.ok(userService.getUserEntity(userPrincipal.getUserId()));
    }

    @GetMapping(path = "/getFiles", produces = {"application/json"})
    public ResponseEntity<String> getAllUserFiles(@AuthenticationPrincipal UserPrincipal userPrincipal,
                                                  @RequestParam(defaultValue = "createdDate") String sortBy,
                                                  @RequestParam(required = false) String order) throws ServiceException {
        List<FileInfoDto> allUserFiles = uploadService.getAllUserFiles(userService.getUserEntity(userPrincipal.getUserId()), sortBy, order);
        return ResponseEntity.ok(new Gson().toJson(allUserFiles));
    }

    @PostMapping(path = "/upload", produces = {"application/json"}) //todo добавить отправку через rabitMq, что загружено изображеие и общий размер изображений
    public ResponseEntity<String> uploadFiles(@AuthenticationPrincipal UserPrincipal userPrincipal,
                                              @RequestParam("files") MultipartFile[] files) throws ServiceException {
        UserEntity user = userService.getUserEntity(userPrincipal.getUserId());
        uploadService.uploadImages(user, files);
        return ResponseEntity.ok("Files uploaded successfully");
    }

    @GetMapping(path = "/download/{fileName}",produces = {"image/png", "image/jpeg"})
    public ResponseEntity<byte[]> downloadFiles(@AuthenticationPrincipal UserPrincipal userPrincipal,
                                                @PathVariable String fileName) throws ServiceException {
        UserEntity user = userService.getUserEntity(userPrincipal.getUserId());
        Map<String, Object> stringObjectMap = uploadService.downloadImage(user, fileName);
        return ResponseEntity.ok()
        .contentType(MediaType.parseMediaType((String) stringObjectMap.get("mimeType")))
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
        .body((byte[]) stringObjectMap.get("content"));

    }
}
