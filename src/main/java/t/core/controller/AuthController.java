package t.core.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import t.core.dto.LoginDtoResponse;
import t.core.dto.LoginRequest;
import t.core.service.AuthService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class AuthController {

    private final AuthService authService;

    @PostMapping("/auth/login")
    public LoginDtoResponse login(@RequestBody @Validated LoginRequest loginRequest){
        return authService.attemptLogin(loginRequest.getEmail(),loginRequest.getPassword());
    }
}
