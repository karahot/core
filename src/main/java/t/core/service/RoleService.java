package t.core.service;


import t.core.entity.Role;
import t.core.exception.ServiceException;

public interface RoleService {

    Role getRoleByName(String roleName) throws ServiceException;
}
