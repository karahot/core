package t.core.service;

import org.springframework.web.multipart.MultipartFile;
import t.core.dto.FileInfoDto;
import t.core.entity.FileInfo;
import t.core.entity.UserEntity;
import t.core.exception.ServiceException;

import java.util.List;
import java.util.Map;

public interface UploadService {
    void uploadImages(UserEntity user, MultipartFile[] files) throws ServiceException;

    Map<String,Object> downloadImage(UserEntity user, String fileName);

    List<FileInfoDto> getAllUserFiles(UserEntity userEntity, String sortBy, String order);
}
