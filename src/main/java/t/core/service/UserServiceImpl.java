package t.core.service;


import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import t.core.dto.UserDto;
import t.core.entity.UserEntity;
import t.core.exception.ServiceErrorCode;
import t.core.exception.ServiceException;
import t.core.repository.UserRepo;

import java.util.List;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;
    private final RoleService roleService;
    private final RabbitTemplate rabbitTemplate;
    private final Binding registrationBinding;

    @Transactional
    @Override
    public String addUserEntity(UserDto userDto) throws ServiceException {
        if(!userRepo.existsByEmail(userDto.getEmail())){
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            UserEntity user = UserEntity.builder()
                        .role(roleService.getRoleByName("ROLE_USER"))
                        .name(userDto.getName())
                        .lastname(userDto.getLastname())
                        .password(bCryptPasswordEncoder.encode(userDto.getPassword()))
                        .patronymic(userDto.getPatronymic())
                        .birthday(userDto.getBirthday())
                        .email(userDto.getEmail())
                        .phoneNumber(userDto.getPhoneNumber())
                        .disable(false).build();
            userRepo.save(user);
            rabbitTemplate.convertAndSend(registrationBinding.getExchange(),registrationBinding.getRoutingKey(), user.getEmail());
            return user.getEmail();
        } else{
            throw new ServiceException(ServiceErrorCode.ERROR_CODE_EMAIL_ALREADY_USE);
        }

    }

    @Transactional
    @Override
    public void removeUserEntity(UUID userIdFromUserPrincipal) { // todo сделать logout, токен в black лист + в контроллер добавить logout
        userRepo.deleteById(userIdFromUserPrincipal);
    }

    @Transactional
    @Override
    public UserEntity getUserEntity(UUID userIdFromUserPrincipal) throws ServiceException {
        return userRepo.findById(userIdFromUserPrincipal)
                .orElseThrow(()-> new ServiceException(ServiceErrorCode.ERROR_CODE_NOT_FOUND));
    }

    @Transactional
    @Override
    public UserEntity findUserEntityByEmail(String username) throws ServiceException {
        return userRepo.findByEmailAndDisable(username, false)
                .orElseThrow(() -> new ServiceException(ServiceErrorCode.ERROR_CODE_NOT_FOUND));
    }

    @Transactional
    @Override
    public List<UserEntity> getAllUserWithStatus(Boolean disable) {
        if(disable != null){
            return userRepo.findAllByDisable(disable);
        } else {
            return userRepo.findAll();
        }
    }

    @Transactional
    @Override
    public void disableUser(String userEmail) throws ServiceException {
        UserEntity user = userRepo.findByEmail(userEmail)
                .orElseThrow(() ->new ServiceException(ServiceErrorCode.ERROR_CODE_NOT_FOUND));
        user.setDisable(true);
    }

}
