package t.core.service;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import t.core.entity.Role;
import t.core.entity.UserEntity;
import t.core.repository.RoleRepo;
import t.core.repository.UserRepo;

import java.time.LocalDate;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class InitDBService {

    private final RoleRepo roleRepository;

    private final UserRepo userRepo;

    @PostConstruct
    public void init() {
        if (roleRepository.findByName("ROLE_ADMIN").isEmpty()) {
            Role adminRole = new Role();
            adminRole.setName("ROLE_ADMIN");
            roleRepository.save(adminRole);
        }
        if (roleRepository.findByName("ROLE_USER").isEmpty()) {
            Role userRole = new Role();
            userRole.setName("ROLE_USER");
            roleRepository.save(userRole);
        }
        if (userRepo.findByEmail("admin").isEmpty()) {
            UserEntity adminUser = new UserEntity();
            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            adminUser.setName("admin");
            adminUser.setLastname("admin");
            adminUser.setPatronymic("admin");
            adminUser.setBirthday(LocalDate.of(2024, 6, 18));
            adminUser.setPassword(passwordEncoder.encode("admin"));
            adminUser.setEmail("admin");
            adminUser.setPhoneNumber("admin");
            Role adminRole = roleRepository.findByName("ROLE_ADMIN").get();
            adminUser.setRole(adminRole);
            userRepo.save(adminUser);
        }
    }
}
