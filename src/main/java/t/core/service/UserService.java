package t.core.service;
import t.core.dto.UserDto;
import t.core.entity.UserEntity;
import t.core.exception.ServiceException;

import java.util.List;
import java.util.UUID;

public interface UserService {

    String addUserEntity(UserDto userDto) throws ServiceException;

    void removeUserEntity(UUID userIdFromUserPrincipal) throws ServiceException;

    UserEntity getUserEntity(UUID userIdFromUserPrincipal) throws ServiceException;

    UserEntity findUserEntityByEmail(String username) throws ServiceException;

    List<UserEntity> getAllUserWithStatus(Boolean disable);

    void disableUser(String userEmail) throws ServiceException;
}
