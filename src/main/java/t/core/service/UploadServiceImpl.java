package t.core.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import t.core.dto.FileInfoDto;
import t.core.entity.FileInfo;
import t.core.entity.UserEntity;
import t.core.exception.ServiceErrorCode;
import t.core.exception.ServiceException;
import t.core.repository.FileInfoRepo;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UploadServiceImpl implements UploadService {


    private final AmazonS3 amazonS3;

    private final FileInfoRepo fileInfoRepo;

    @Value("${yandexCloud.bucketName}")
    private String bucketName;

    private final RabbitTemplate rabbitTemplate;
    private final Binding uploadBinding;


    @Override
    @Async
    public void uploadImages(UserEntity user, MultipartFile[] files) throws ServiceException {
        Long fileSize = 0L;
        try {
        for (MultipartFile file : files) {
            if(((file.getContentType().equals("image/jpeg"))||(file.getContentType().equals("image/png")))&&(file.getSize() < (10 * 1024 * 1024))){
                String fileName = file.getOriginalFilename();
                InputStream inputStream = file.getInputStream();
                ObjectMetadata metadata = new ObjectMetadata();
                metadata.addUserMetadata("uploaded-by", user.getEmail());
                amazonS3.putObject(new PutObjectRequest(bucketName, user.getId().toString() + "/" + fileName, inputStream, metadata)
                        .withCannedAcl(CannedAccessControlList.PublicRead));
                fileSize = fileSize+file.getSize();

                FileInfo fileInfo = FileInfo.builder()
                        .fileName(file.getOriginalFilename())
                        .weight(file.getSize())
                        .loadDate(LocalDateTime.now())
                        .savedBy(user)
                        .build();
                fileInfoRepo.save(fileInfo);

                Map<String, String> emailAndSize = new HashMap<>();
                emailAndSize.put("email", user.getEmail());
                emailAndSize.put("size", String.valueOf(fileSize));
                rabbitTemplate.convertAndSend(uploadBinding.getExchange(),uploadBinding.getRoutingKey(), emailAndSize);
            } else {
                throw new ServiceException(ServiceErrorCode.ERROR_CODE_WRONG_FILE);
            }
        }
        } catch (IOException e) {
            throw new ServiceException(ServiceErrorCode.ERROR_CODE_IO_EXCEPTION);
        }
    }

    @Override
    public List<FileInfoDto> getAllUserFiles(UserEntity userEntity, String sortBy, String order) {
        List<FileInfo> userFilesByUser = fileInfoRepo.findBySavedBy(userEntity);
        if (!userFilesByUser.isEmpty()) {
            Comparator<FileInfo> comparator = getComparator(sortBy, order);
            userFilesByUser = userFilesByUser.stream()
                    .sorted(comparator)
                    .collect(Collectors.toList());
        }
        List<FileInfoDto> fileInfoDto = convertFileInfoDto(userFilesByUser);
        return fileInfoDto;
    }

    private List<FileInfoDto> convertFileInfoDto(List<FileInfo> fileInfos){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return fileInfos.stream()
                .map(fileInfo -> FileInfoDto.builder()
                        .id(fileInfo.getId())
                        .weight(fileInfo.getWeight())
                        .email(fileInfo.getSavedBy().getEmail())
                        .fileName(fileInfo.getFileName())
                        .loadDate(fileInfo.getLoadDate().format(formatter)).build()
                )
                .collect(Collectors.toList());
    }

    private Comparator<FileInfo> getComparator(String sortBy, String order) {
        Comparator<FileInfo> comparator;
        switch (sortBy) {
            case "name":
                comparator = Comparator.comparing(FileInfo::getFileName);
                break;
            case "size":
                comparator = Comparator.comparingLong(FileInfo::getWeight);
                break;
            case "createdDate":
                comparator = Comparator.comparing(FileInfo::getLoadDate);
                break;
            default:
                throw new IllegalArgumentException("Invalid sortBy parameter: " + sortBy);
        }
        if ("desc".equalsIgnoreCase(order)) {
            comparator = comparator.reversed();
        }
        return comparator;
    }

    @Override
    public Map<String,Object> downloadImage(UserEntity user,String fileName) {
        S3Object s3Object = amazonS3.getObject(bucketName, user.getId() + "/" + fileName);
        Map<String, Object> contentMimeType = new HashMap<>();
        try (InputStream inputStream = s3Object.getObjectContent()) {
            byte[] content = inputStream.readAllBytes();
            contentMimeType.put("content", content);
            contentMimeType.put("mimeType", s3Object.getObjectMetadata().getContentType());
            return contentMimeType;
        } catch (IOException e) {
            System.out.println("Errrorororo");
        }
        return contentMimeType;
    }


}
