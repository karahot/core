package t.core.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import t.core.entity.Role;
import t.core.exception.ServiceErrorCode;
import t.core.exception.ServiceException;
import t.core.repository.RoleRepo;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepo roleRepo;

    @Transactional
    @Override
    public Role getRoleByName(String roleName) throws ServiceException {
        return roleRepo.findByName(roleName)
                .orElseThrow(()-> new ServiceException(ServiceErrorCode.ERROR_CODE_NOT_FOUND));
    }
}
