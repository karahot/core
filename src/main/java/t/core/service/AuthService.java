package t.core.service;


import t.core.dto.LoginDtoResponse;

public interface AuthService {
    LoginDtoResponse attemptLogin(String email, String password);
}
