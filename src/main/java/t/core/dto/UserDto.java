package t.core.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.*;

import java.time.LocalDate;
@Data
public class UserDto {

        @NotBlank
        private String name;

        @NotBlank
        private String lastname;

        @NotBlank
        private String patronymic;

        @NotNull
        private LocalDate birthday;

        @NotBlank
        private String password;

        @Email
        private String email;

        @Pattern(regexp = "^\\+7\\d{10}$", message = "Phone number must be in the format +7XXXXXXXXXX")
        private String phoneNumber;

}
