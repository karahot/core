package t.core.dto;

import jakarta.persistence.*;
import lombok.*;
import t.core.entity.UserEntity;

import java.time.LocalDateTime;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class FileInfoDto {


    private UUID id;

    private String fileName;

    private long weight;

    private String loadDate;

    private String email;
}
