package t.core.rabbit;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.amqp.support.converter.DefaultJackson2JavaTypeMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.List;

@Configuration
public class RabbitMQConfig {


    @Value("${rabbitmq.exchange}")
    private String exchange;

    @Value("${rabbitmq.routingkey.registration}")
    private String routingKeyRegistration;

    @Value("${rabbitmq.routingkey.upload}")
    private String routingKeyUpload;

    @Bean
    public Queue registrationQueue() {
        return new Queue("registrationQueue", false);
    }

    @Bean
    public Queue uploadQueue() {
        return new Queue("uploadQueue", false);
    }

    @Bean
    Exchange exchange() {
        return new TopicExchange(exchange, false, false);
    }
    @Bean

    Binding registrationBinding(Queue registrationQueue, Exchange exchange) {
        return BindingBuilder.bind(registrationQueue).to(exchange).with(routingKeyRegistration).noargs();
    }

    @Bean
    Binding uploadBinding(Queue uploadQueue, Exchange exchange) {
        return BindingBuilder.bind(uploadQueue).to(exchange).with(routingKeyUpload).noargs();
    }

    @Bean
    public RabbitListenerContainerFactory<?> rabbitListenerContainerFactory(ConnectionFactory connectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setMessageConverter(simpleMessageConverter());
        return factory;
    }

    @Bean
    public SimpleMessageConverter simpleMessageConverter() {
        SimpleMessageConverter converter = new SimpleMessageConverter();
        converter.setAllowedListPatterns(List.of("src.main.java.t.mail.*", "java.util.HashMap","java.lang.String"));
        return converter;
    }
}
