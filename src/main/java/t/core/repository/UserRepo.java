package t.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import t.core.entity.UserEntity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepo extends JpaRepository<UserEntity, UUID> {

    boolean existsByEmail(String email);

    Optional<UserEntity> findByEmailAndDisable(String email,boolean disable);

    List<UserEntity> findAllByDisable(boolean disable);

    Optional<UserEntity> findByEmail(String userEmail);
}
