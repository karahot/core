package t.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import t.core.entity.FileInfo;
import t.core.entity.UserEntity;

import java.util.List;
import java.util.UUID;

public interface FileInfoRepo extends JpaRepository<FileInfo, UUID> {


    List<FileInfo> findBySavedBy(UserEntity user);


}
