package t.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import t.core.entity.Role;

import java.util.Optional;
import java.util.UUID;

public interface RoleRepo extends JpaRepository<Role, UUID> {

    Optional<Role> findByName(String name);
}
