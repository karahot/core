package t.core.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public enum ServiceErrorCode {

    ERROR_CODE_EMAIL_ALREADY_USE("Email is used already", HttpStatus.CONFLICT),
    ERROR_CODE_NOT_FOUND("Not found",HttpStatus.NOT_FOUND),
    ERROR_CODE_INCORRECT_ID("Incorrect id", HttpStatus.CONFLICT),
    ERROR_CODE_IO_EXCEPTION("IO ex", HttpStatus.CONFLICT),
    ERROR_CODE_WRONG_FILE("File must be less than 10 MB and the format must be jpg or png ", HttpStatus.BAD_REQUEST);

    private final String errorString;
    private final  HttpStatus httpStatus;

    ServiceErrorCode (String errorString, HttpStatus httpStatus){
        this.errorString = errorString;
        this.httpStatus = httpStatus;
    }

}
