package t.core.exception;

import lombok.Getter;

@Getter
public class ServiceException extends Exception{

    private final ServiceErrorCode SERVICE_ERROR_CODE;

    public ServiceException(ServiceErrorCode serviceErrorCode){
        this.SERVICE_ERROR_CODE = serviceErrorCode;
    }

}
