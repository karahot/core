package t.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ServiceException.class)
    public ResponseEntity<Object> handleServiceException(ServiceException ex) {
        Map<String, Object> body = new HashMap<>();
        body.put("time", LocalDate.now());
        body.put("error", ex.getSERVICE_ERROR_CODE().getErrorString());
        body.put("message", ex.getMessage());
        return new ResponseEntity<>(body, ex.getSERVICE_ERROR_CODE().getHttpStatus());
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleOtherException(Exception ex) {
        Map<String, Object> body = new HashMap<>();
        body.put("time", LocalDate.now());
        body.put("error", "Server error");
        body.put("message", ex.getMessage());
        return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
