package t.core.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class Role {

    @Id
    @GeneratedValue
    UUID id;

    String name;

    @OneToMany
    @JoinTable(
            name = "role_user",
            joinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns =@JoinColumn(name = "user_id", referencedColumnName = "id"))
    List<UserEntity> userEntities;
}
