package t.core.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@Entity
public class FileInfo {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(name = "file_name")
    private String fileName;

    @Column
    private long weight;

    @Column(name = "load_date")
    private LocalDateTime loadDate;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity savedBy;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileInfo fileInfo = (FileInfo) o;
        return weight == fileInfo.weight && Objects.equals(fileName, fileInfo.fileName) && Objects.equals(loadDate, fileInfo.loadDate) && Objects.equals(savedBy, fileInfo.savedBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileName, weight, loadDate, savedBy);
    }
}
